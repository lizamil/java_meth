package convert;

public class Main {
    public static void main(String[] args) {
        conToStr(10);
        conToInt("255");
    }

    public static String conToStr(int x) {
        String str = "" + x;
        System.out.println(str);
        return str;
    }

    public static int conToInt(String st) {
        int a = Integer.parseInt(st);
        System.out.println(a);
        return a;
    }
}
