package palindrome;


public class Methods {

    public static String reverseString(String pal) {
        char[] strtochar = pal.toCharArray();
        int len = strtochar.length-1;
        char temp = strtochar[0];
        for (int i = 0; i < len; i++) {
            strtochar[i] = strtochar[len- i];
            strtochar[len- i] = temp;

            temp = strtochar[i+1];
        }
        String pal1 = new String();
        return pal1 = new String(strtochar);

    }

    public static boolean isPalindrome(String pal) {
        String a = reverseString(pal);
        return a.equals(pal);
    }

    public static void printOut(String pal) {
        System.out.println(pal);
        System.out.println(isPalindrome(pal));
    }
}
